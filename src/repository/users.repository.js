'use strict'

const Users = require('../model/users.model')()

exports.findAllUsers = async () => {
    return await Users.findAll({});
}

exports.findUserById = async (id) => {
    return await Users.findById(id)
}

exports.update = async (data) => {
    return await Users.update({
        name: data.name,
        email: data.email,
        password: data.password
    }, {
        where: { id: data.id }
    });
}

exports.deleteById = async (id) => {
    return await Users.destroy({
        where: {
            id: id
        }
    })
}

exports.create = async (data) => {
    return await Users.create(data);
}

