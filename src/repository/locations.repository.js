'use strict'

const Locations = require('../model/location.model')()

exports.findAllLocations = async () => {
    return await Locations.findAll({});
}

exports.findLocationsByUserId = async (id) => {
    return await Locations.findAll({
        where: {
            userId: id
        }
    });
}

exports.create = async (data)=>{
    return await Locations.create(data);
}

