'use strict'

const Sequelize = require('sequelize');
const model = require("./model")
const data = {
    latitude: {
        type: Sequelize.DECIMAL
    },
    longitude: {
        type: Sequelize.DECIMAL
    },
    userId: {
        type: Sequelize.INTEGER,
        references: {
            model: 'users',
            key: 'id'
        }
    }
};

module.exports = () => {
    let Locations = model('locations', data);
    Locations.associate = function (models) {
        Locations.belongsTo(models.users, { foreignKey: 'userId' })
    };
    return Locations;
}
