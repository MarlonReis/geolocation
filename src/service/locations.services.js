'use strict'

const repository = require('../repository/locations.repository');

exports.create = async (req, res, next) => {
    const response = await repository.create(req.body);
    res.status(201).send(response);
}

exports.findAllLocations = async (req, res, next) => {
    const response = await repository.findAllLocations();
    res.status(200).send(response);
}

exports.findLocationsByUserId = async (req, res, next) => {
    const response = await repository.findLocationsByUserId(req.params.id);
    res.status(200).send(response);
}