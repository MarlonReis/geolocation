'use strict';

const repository = require('../repository/users.repository')

exports.create = async (req, res, next) => {
    const response = await repository.create(req.body)
    res.status(201).send(response)
}

exports.update = async (req, res, next) => {
    const response = await repository.update(req.body)
    res.status(200).send(response)
}

exports.findAllUsers = async (req, res, next) => {
    const response = await repository.findAllUsers();
    res.status(200).send(response)
}

exports.findUserById = async (req, res, next) => {
    const response = await repository.findUserById(req.params.id);
    res.status(200).send(response)
}

exports.deleteById = async (req, res, next) => {
    const response = await repository.deleteById(req.params.id);
    res.status(200).send(response)
}