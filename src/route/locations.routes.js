'use strict'

'use strict'

const router = require('./route');
const service = require('../service/locations.services');

router.get('/locations', service.findAllLocations);
router.post('/locations', service.create);
router.get('/locations/user/:id', service.findLocationsByUserId);

module.exports = router