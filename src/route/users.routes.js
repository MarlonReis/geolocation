'use strict'

const router = require('./route');
const service = require('../service/users.service');

router.get('/users', service.findAllUsers);
router.post('/users', service.create);
router.put('/users', service.update);
router.get('/users/:id', service.findUserById);
router.delete('/users/:id', service.deleteById);

module.exports = router

