'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
require('dotenv').config();

const connection = require('./connection/connnection');
connection.start();

const usersRouters = require('./route/users.routes');
const locationsRouters = require('./route/locations.routes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('geolocation/api/v1', usersRouters);
app.use('geolocation/api/v1', locationsRouters);


module.exports = app;
